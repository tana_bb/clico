#pragma once

#include "cocos2d.h"


// スクロール状態
class ScrollStatus : public cocos2d::Object
{
private:
	cocos2d::Vec2 m_origin;  // 原点座標

	// コンストラクタ
	ScrollStatus()
		: cocos2d::Object()
		, m_origin(0.0F, 0.0F)
	{
	}


public:
	// ファクトリメソッド
	CREATE_FUNC(ScrollStatus);

	// 初期化
	virtual bool init();


	// 原点座標を取得
	cocos2d::Vec2 getOrigin() const;

	// 原点座標を設定
	void setOrigin(cocos2d::Vec2 value);
};
