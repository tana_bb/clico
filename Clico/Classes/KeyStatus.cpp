#include "KeyStatus.h"

using namespace cocos2d;


// 初期化
bool KeyStatus::init()
{
	return true;
}


// キーの押下状態を取得
bool KeyStatus::isKeyDown(EventKeyboard::KeyCode key)
{
	return m_keyMap[key];
}


// キーの押下状態を設定
void KeyStatus::setKeyDown(EventKeyboard::KeyCode key, bool value)
{
	m_keyMap[key] = value;
}
