#pragma once

#include "cocos2d.h"
#include "chipmunk.h"

#include <map>


#define FLAG_MAX 0x80000000


// キャラクターのベースクラス
class Character : public cocos2d::Sprite
{
protected:
	std::map<int, cocos2d::Sprite *> m_sprites;  // スプライトを格納
	std::map<int, cocos2d::Action *> m_actions;  // アクションを格納
	std::map<int, std::map<int, cocos2d::Action *>> m_animations;  // スプライト毎のアニメーションを格納

	int m_status;  // 現在の状態

	int m_currentAction;  // 現在のアクション
	int m_currentAnimation;  // 現在のアニメーション

	float m_attackValue;  // 攻撃力

	// コンストラクタ
	Character()
		: cocos2d::Sprite()
		, m_sprites()
		, m_actions()
		, m_animations()
		, m_status(0)
		, m_currentAction(0)
		, m_currentAnimation(0)
		, m_attackValue(0)
	{
	}


public:
	// ファクトリメソッド
	CREATE_FUNC(Character);

	// デストラクタ
	virtual ~Character();

	// 初期化
	virtual bool init();


	// アクションを追加
	void addAction(int types);

	// アクションを除去
	void removeAction(int types);
	

	// アニメーションを設定
	void setAnimation(int type);


	// 状態を追加
	virtual void addStatus(int types);
	
	// 状態を除去
	virtual void removeStatus(int types);
};
