#pragma once

#include "cocos2d.h"
#include "Character.h"


enum class ClicoTentacleSprite
{
	TENTACLE1,
	TENTACLE2,
	TENTACLE3,
	TENTACLE4,
	TENTACLE5,
	TENTACLE6
};


enum class ClicoTentacleAction
{
};


enum class ClicoTentacleAnimation
{
	NORMAL = 1,
	EAT
};


enum class ClicoTentacleStatus
{
	EAT = 0x1
};


// クリ子ちゃんの触手
class ClicoTentacle : public Character
{
private:
	// コンストラクタ
	ClicoTentacle()
		: Character()
	{
	}


protected:
	// 捕食状態のハンドラ
	virtual void func_eat(float dt);


public:
	// ファクトリメソッド
	CREATE_FUNC(ClicoTentacle);

	// デストラクタ
	~ClicoTentacle();


	// 初期化
	virtual bool init();


	// 状態を追加
	virtual void addStatus(int types);
	
	// 状態を除去
	virtual void removeStatus(int types);
};
