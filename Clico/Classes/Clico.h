#pragma once

#include "cocos2d.h"
#include "Character.h"
#include "MouseStatus.h"
#include "KeyStatus.h"
#include "ScrollStatus.h"
#include "ClicoTentacle.h"


enum class ClicoSprite
{
	FIN,
	HEAD,
	BODY,
	HEART,
	CHEEK,
	EYE
};


enum class ClicoAction
{
};


enum class ClicoAnimation
{
	NORMAL = 1,
	MOVE,
	EAT
};


enum class ClicoStatus
{
	CONTROLLABLE = 0x1,
	DAMAGE = 0x2,
	EAT = 0x4
};


// クリ子ちゃん
class Clico : public Character
{
private:
	MouseStatus *m_mouseStatus;  // マウス状態
	KeyStatus *m_keyStatus;  // キー状態
	ScrollStatus *m_scrollStatus;  // スクロール状態

	ClicoTentacle *m_tentacle;  // 触手

	float m_moveVelLimit;  // 移動の限界速度
	float m_moveAccel;  // 移動の加速度
	float m_moveRotLimit;  // 回転の限界速度
	float m_moveRotSpeed;  // 回転の速度
	float m_stopAccel;  // 停止の加速度

	// コンストラクタ
	Clico()
		: Character()
		, m_mouseStatus(nullptr)
		, m_keyStatus(nullptr)
		, m_scrollStatus(nullptr)
		, m_tentacle(nullptr)
		, m_moveVelLimit(500.0F)
		, m_moveAccel(1500.0F)
		, m_moveRotLimit(4.0F * M_PI)
		, m_moveRotSpeed(720.0F)
		, m_stopAccel(2500.0F)
	{
	}


protected:
	// PhysicsContactのイベントハンドラ
	virtual bool onPhysicsContact(cocos2d::PhysicsContact &contact);

	// 操作可能状態のハンドラ
	virtual void func_controllable(float dt);

	// 捕食状態のハンドラ
	virtual void func_eat(float dt);


public:
	// ファクトリメソッド
	CREATE_FUNC(Clico);

	// デストラクタ
	~Clico();
	

	// 初期化
	virtual bool init();


	// 状態を追加
	virtual void addStatus(int types);

	// 状態を除去
	virtual void removeStatus(int types);


	// マウス状態を設定
	void setMouseStatus(MouseStatus *status);
	
	// キー状態を設定
	void setKeyStatus(KeyStatus *status);
	
	// スクロール状態を設定
	void setScrollStatus(ScrollStatus *status);
};
