#include "ScrollStatus.h"

using namespace cocos2d;


// 初期化
bool ScrollStatus::init()
{
	return true;
}


// 原点座標を取得
Vec2 ScrollStatus::getOrigin() const
{
	return m_origin;
}


// 原点座標を設定
void ScrollStatus::setOrigin(cocos2d::Vec2 value)
{
	m_origin = value;
}
