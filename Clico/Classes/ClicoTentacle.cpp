#include "ClicoTentacle.h"

using namespace cocos2d;


// デストラクタ
ClicoTentacle::~ClicoTentacle()
{
}


// 初期化
bool ClicoTentacle::init()
{
	if (!Character::init())
	{
		return false;
	}

	// マスクを作成
	auto clip = ClippingNode::create(Sprite::create("clione/clione_tentacle_mask.png"));
	clip->setAlphaThreshold(0.0F);
	this->addChild(clip);

	// スプライトを作成
	m_sprites[(int)ClicoTentacleSprite::TENTACLE1] = Sprite::create("clione/clione_tentacle_01.png");
	m_sprites[(int)ClicoTentacleSprite::TENTACLE1]->retain();
	m_sprites[(int)ClicoTentacleSprite::TENTACLE1]->setTag((int)ClicoTentacleSprite::TENTACLE1);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE1]->setAnchorPoint(Vec2(1.0F, 0.0F));
	m_sprites[(int)ClicoTentacleSprite::TENTACLE1]->setPosition(30.0F, -20.0F);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE1]->setRotation(-60);
	clip->addChild(m_sprites[(int)ClicoTentacleSprite::TENTACLE1], 0);

	m_sprites[(int)ClicoTentacleSprite::TENTACLE2] = Sprite::create("clione/clione_tentacle_01.png");
	m_sprites[(int)ClicoTentacleSprite::TENTACLE2]->retain();
	m_sprites[(int)ClicoTentacleSprite::TENTACLE2]->setTag((int)ClicoTentacleSprite::TENTACLE2);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE2]->setAnchorPoint(Vec2(1.0F, 0.0F));
	m_sprites[(int)ClicoTentacleSprite::TENTACLE2]->setPosition(30.0F, -20.0F);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE2]->setRotation(-60);
	clip->addChild(m_sprites[(int)ClicoTentacleSprite::TENTACLE2], 0);

	m_sprites[(int)ClicoTentacleSprite::TENTACLE3] = Sprite::create("clione/clione_tentacle_01.png");
	m_sprites[(int)ClicoTentacleSprite::TENTACLE3]->retain();
	m_sprites[(int)ClicoTentacleSprite::TENTACLE3]->setTag((int)ClicoTentacleSprite::TENTACLE3);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE3]->setAnchorPoint(Vec2(1.0F, 0.0F));
	m_sprites[(int)ClicoTentacleSprite::TENTACLE3]->setPosition(30.0F, -20.0F);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE3]->setRotation(-60);
	clip->addChild(m_sprites[(int)ClicoTentacleSprite::TENTACLE3], 0);

	m_sprites[(int)ClicoTentacleSprite::TENTACLE4] = Sprite::create("clione/clione_tentacle_02.png");
	m_sprites[(int)ClicoTentacleSprite::TENTACLE4]->retain();
	m_sprites[(int)ClicoTentacleSprite::TENTACLE4]->setTag((int)ClicoTentacleSprite::TENTACLE4);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE4]->setAnchorPoint(Vec2(0.0F, 0.0F));
	m_sprites[(int)ClicoTentacleSprite::TENTACLE4]->setPosition(-30.0F, -20.0F);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE4]->setRotation(60);
	clip->addChild(m_sprites[(int)ClicoTentacleSprite::TENTACLE4], 0);

	m_sprites[(int)ClicoTentacleSprite::TENTACLE5] = Sprite::create("clione/clione_tentacle_02.png");
	m_sprites[(int)ClicoTentacleSprite::TENTACLE5]->retain();
	m_sprites[(int)ClicoTentacleSprite::TENTACLE5]->setTag((int)ClicoTentacleSprite::TENTACLE5);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE5]->setAnchorPoint(Vec2(0.0F, 0.0F));
	m_sprites[(int)ClicoTentacleSprite::TENTACLE5]->setPosition(-30.0F, -20.0F);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE5]->setRotation(60);
	clip->addChild(m_sprites[(int)ClicoTentacleSprite::TENTACLE5], 0);

	m_sprites[(int)ClicoTentacleSprite::TENTACLE6] = Sprite::create("clione/clione_tentacle_02.png");
	m_sprites[(int)ClicoTentacleSprite::TENTACLE6]->retain();
	m_sprites[(int)ClicoTentacleSprite::TENTACLE6]->setTag((int)ClicoTentacleSprite::TENTACLE6);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE6]->setAnchorPoint(Vec2(0.0F, 0.0F));
	m_sprites[(int)ClicoTentacleSprite::TENTACLE6]->setPosition(-30.0F, -20.0F);
	m_sprites[(int)ClicoTentacleSprite::TENTACLE6]->setRotation(60);
	clip->addChild(m_sprites[(int)ClicoTentacleSprite::TENTACLE6], 0);

	// アニメーションを作成
	auto anime1_eat1 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, 0.0F));
	auto anime1_eat2 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, -30.0F));
	auto anime1_eat_seq1 = Sequence::create(anime1_eat1, anime1_eat2, nullptr);
	auto anime1_eat3 = RotateBy::create(20.0F / 60.0F, 120.0F);
	auto anime1_eat4 = RotateBy::create(10.0F / 60.0F, 60.0F);
	auto anime1_eat_seq2 = Sequence::create(anime1_eat3, anime1_eat4, nullptr);
	auto anime1_eat_spawn = Spawn::create(anime1_eat_seq1, anime1_eat_seq2, nullptr);
	auto anime1_eat_func = CallFunc::create([&]() -> void {
		m_sprites[(int)ClicoTentacleSprite::TENTACLE1]->setPosition(30.0F, -20.0F);
		m_sprites[(int)ClicoTentacleSprite::TENTACLE1]->setRotation(-60.0F);
	});
	auto anime1_eat_seq3 = Sequence::create(anime1_eat_spawn, anime1_eat_func, nullptr);
	m_animations[(int)ClicoTentacleSprite::TENTACLE1][(int)ClicoTentacleAnimation::EAT] = anime1_eat_seq3;
	m_animations[(int)ClicoTentacleSprite::TENTACLE1][(int)ClicoTentacleAnimation::EAT]->retain();

	auto anime2_dly = DelayTime::create(20.0F / 60.0F);
	auto anime2_eat1 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, 0.0F));
	auto anime2_eat2 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, -30.0F));
	auto anime2_eat_seq1 = Sequence::create(anime2_dly, anime2_eat1, anime2_eat2, nullptr);
	auto anime2_eat3 = RotateBy::create(20.0F / 60.0F, 120.0F);
	auto anime2_eat4 = RotateBy::create(10.0F / 60.0F, 60.0F);
	auto anime2_eat_seq2 = Sequence::create(anime2_dly, anime2_eat3, anime2_eat4, nullptr);
	auto anime2_eat_spawn = Spawn::create(anime2_eat_seq1, anime2_eat_seq2, nullptr);
	auto anime2_eat_func = CallFunc::create([&]() -> void {
		m_sprites[(int)ClicoTentacleSprite::TENTACLE2]->setPosition(30.0F, -20.0F);
		m_sprites[(int)ClicoTentacleSprite::TENTACLE2]->setRotation(-60.0F);
	});
	auto anime2_eat_seq3 = Sequence::create(anime2_eat_spawn, anime2_eat_func, nullptr);
	m_animations[(int)ClicoTentacleSprite::TENTACLE2][(int)ClicoTentacleAnimation::EAT] = anime2_eat_seq3;
	m_animations[(int)ClicoTentacleSprite::TENTACLE2][(int)ClicoTentacleAnimation::EAT]->retain();

	auto anime3_dly = DelayTime::create(40.0F / 60.0F);
	auto anime3_eat1 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, 0.0F));
	auto anime3_eat2 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, -30.0F));
	auto anime3_eat_seq1 = Sequence::create(anime3_dly, anime3_eat1, anime3_eat2, nullptr);
	auto anime3_eat3 = RotateBy::create(20.0F / 60.0F, 120.0F);
	auto anime3_eat4 = RotateBy::create(10.0F / 60.0F, 60.0F);
	auto anime3_eat_seq2 = Sequence::create(anime3_dly, anime3_eat3, anime3_eat4, nullptr);
	auto anime3_eat_spawn = Spawn::create(anime3_eat_seq1, anime3_eat_seq2, nullptr);
	auto anime3_eat_func = CallFunc::create([&]() -> void {
		m_sprites[(int)ClicoTentacleSprite::TENTACLE3]->setPosition(30.0F, -20.0F);
		m_sprites[(int)ClicoTentacleSprite::TENTACLE3]->setRotation(-60.0F);
	});
	auto anime3_eat_seq3 = Sequence::create(anime3_eat_spawn, anime3_eat_func, nullptr);
	m_animations[(int)ClicoTentacleSprite::TENTACLE3][(int)ClicoTentacleAnimation::EAT] = anime3_eat_seq3;
	m_animations[(int)ClicoTentacleSprite::TENTACLE3][(int)ClicoTentacleAnimation::EAT]->retain();

	auto anime4_dly = DelayTime::create(10.0F / 60.0F);
	auto anime4_eat1 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, 0.0F));
	auto anime4_eat2 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, -30.0F));
	auto anime4_eat_seq1 = Sequence::create(anime4_dly, anime4_eat1, anime4_eat2, nullptr);
	auto anime4_eat3 = RotateBy::create(20.0F / 60.0F, -120.0F);
	auto anime4_eat4 = RotateBy::create(10.0F / 60.0F, -60.0F);
	auto anime4_eat_seq2 = Sequence::create(anime4_dly, anime4_eat3, anime4_eat4, nullptr);
	auto anime4_eat_spawn = Spawn::create(anime4_eat_seq1, anime4_eat_seq2, nullptr);
	auto anime4_eat_func = CallFunc::create([&]() -> void {
		m_sprites[(int)ClicoTentacleSprite::TENTACLE4]->setPosition(-30.0F, -20.0F);
		m_sprites[(int)ClicoTentacleSprite::TENTACLE4]->setRotation(60.0F);
	});
	auto anime4_eat_seq3 = Sequence::create(anime4_eat_spawn, anime4_eat_func, nullptr);
	m_animations[(int)ClicoTentacleSprite::TENTACLE4][(int)ClicoTentacleAnimation::EAT] = anime4_eat_seq3;
	m_animations[(int)ClicoTentacleSprite::TENTACLE4][(int)ClicoTentacleAnimation::EAT]->retain();

	auto anime5_dly = DelayTime::create(30.0F / 60.0F);
	auto anime5_eat1 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, 0.0F));
	auto anime5_eat2 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, -30.0F));
	auto anime5_eat_seq1 = Sequence::create(anime5_dly, anime5_eat1, anime5_eat2, nullptr);
	auto anime5_eat3 = RotateBy::create(20.0F / 60.0F, -120.0F);
	auto anime5_eat4 = RotateBy::create(10.0F / 60.0F, -60.0F);
	auto anime5_eat_seq2 = Sequence::create(anime5_dly, anime5_eat3, anime5_eat4, nullptr);
	auto anime5_eat_spawn = Spawn::create(anime5_eat_seq1, anime5_eat_seq2, nullptr);
	auto anime5_eat_func = CallFunc::create([&]() -> void {
		m_sprites[(int)ClicoTentacleSprite::TENTACLE5]->setPosition(-30.0F, -20.0F);
		m_sprites[(int)ClicoTentacleSprite::TENTACLE5]->setRotation(60.0F);
	});
	auto anime5_eat_seq3 = Sequence::create(anime5_eat_spawn, anime5_eat_func, nullptr);
	m_animations[(int)ClicoTentacleSprite::TENTACLE5][(int)ClicoTentacleAnimation::EAT] = anime5_eat_seq3;
	m_animations[(int)ClicoTentacleSprite::TENTACLE5][(int)ClicoTentacleAnimation::EAT]->retain();

	auto anime6_dly = DelayTime::create(50.0F / 60.0F);
	auto anime6_eat1 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, 0.0F));
	auto anime6_eat2 = MoveTo::create(20.0F / 60.0F, Vec2(0.0F, -30.0F));
	auto anime6_eat_seq1 = Sequence::create(anime6_dly, anime6_eat1, anime6_eat2, nullptr);
	auto anime6_eat3 = RotateBy::create(20.0F / 60.0F, -120.0F);
	auto anime6_eat4 = RotateBy::create(10.0F / 60.0F, -60.0F);
	auto anime6_eat_seq2 = Sequence::create(anime6_dly, anime6_eat3, anime6_eat4, nullptr);
	auto anime6_eat_spawn = Spawn::create(anime6_eat_seq1, anime6_eat_seq2, nullptr);
	auto anime6_eat_func = CallFunc::create([&]() -> void {
		m_sprites[(int)ClicoTentacleSprite::TENTACLE6]->setPosition(-30.0F, -20.0F);
		m_sprites[(int)ClicoTentacleSprite::TENTACLE6]->setRotation(60.0F);
	});
	auto anime6_eat_seq3 = Sequence::create(anime6_eat_spawn, anime6_eat_func, nullptr);
	m_animations[(int)ClicoTentacleSprite::TENTACLE6][(int)ClicoTentacleAnimation::EAT] = anime6_eat_seq3;
	m_animations[(int)ClicoTentacleSprite::TENTACLE6][(int)ClicoTentacleAnimation::EAT]->retain();

	return true;
}


// 状態を追加
void ClicoTentacle::addStatus(int types)
{
	for (auto i = 0x1; i < FLAG_MAX; i <<= 1)
	{
		switch (types & i)
		{
		case (int)ClicoTentacleStatus::EAT:
			m_status |= (int)ClicoTentacleStatus::EAT;
			this->schedule(schedule_selector(ClicoTentacle::func_eat));
			break;
		}
	}
}


// 状態を除去
void ClicoTentacle::removeStatus(int types)
{
	for (auto i = 0x1; i < FLAG_MAX; i <<= 1)
	{
		switch (types & i)
		{
		case (int)ClicoTentacleStatus::EAT:
			m_status &= ~(int)ClicoTentacleStatus::EAT;
			this->unschedule(schedule_selector(ClicoTentacle::func_eat));
			break;
		}
	}
}


// 捕食状態のハンドラ
void ClicoTentacle::func_eat(float dt)
{
	auto isDone = true;

	for (auto &a : m_animations)
	{
		if (a.second[(int)ClicoTentacleAnimation::EAT] != nullptr
			&& !a.second[(int)ClicoTentacleAnimation::EAT]->isDone())
		{
			isDone = false;
		}
	}

	if (isDone)
	{
		removeStatus((int)ClicoTentacleStatus::EAT);
		setAnimation((int)ClicoTentacleAnimation::NORMAL);
	}
}
