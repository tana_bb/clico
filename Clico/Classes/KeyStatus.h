#pragma once

#include "cocos2d.h"
#include <map>


// キー状態
class KeyStatus : public cocos2d::Object
{
private:
	std::map<cocos2d::EventKeyboard::KeyCode, bool> m_keyMap;  // 各キーの押下状態を格納

	// コンストラクタ
	KeyStatus()
		: cocos2d::Object()
		, m_keyMap()
	{
	}


public:
	// ファクトリメソッド
	CREATE_FUNC(KeyStatus);

	// 初期化
	virtual bool init();


	// キーの押下状態を取得
	bool isKeyDown(cocos2d::EventKeyboard::KeyCode key);
	
	// キーの押下状態を設定
	void setKeyDown(cocos2d::EventKeyboard::KeyCode key, bool value);
};
