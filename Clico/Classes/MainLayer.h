#pragma once

#include "cocos2d.h"
#include "MouseStatus.h"
#include "KeyStatus.h"
#include "ScrollStatus.h"


// ゲームのメインレイヤー
class MainLayer : public cocos2d::Layer
{
private:
	cocos2d::PhysicsWorld *m_physicsWorld;  // 物理ワールド

	MouseStatus *m_mouseStatus;  // マウス状態
	KeyStatus *m_keyStatus;  // キー状態
	ScrollStatus *m_scrollStatus;  // スクロール状態

	// コンストラクタ
	MainLayer()
		: cocos2d::Layer()
		, m_physicsWorld(nullptr)
		, m_mouseStatus(nullptr)
		, m_keyStatus(nullptr)
		, m_scrollStatus(nullptr)
	{
	}

	// 物理を初期化
	void initPhysics(cocos2d::PhysicsWorld *physicsWorld);


protected:
	// MouseDownイベントハンドラ
	void onMouseDown(cocos2d::Event *event);
	
	// MouseUpイベントハンドラ
	void onMouseUp(cocos2d::Event *event);
	
	// MouseMoveイベントハンドラ
	void onMouseMove(cocos2d::Event *event);
	
	// MouseScrollイベントハンドラ
	void onMouseScroll(cocos2d::Event *event);


	// KeyPressedイベントハンドラ
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode key, cocos2d::Event *event);
	
	// KeyReleasedイベントハンドラ
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode key, cocos2d::Event *event);


public:
	// シーンを作成
	static cocos2d::Scene *createScene();


	// ファクトリメソッド
	CREATE_FUNC(MainLayer);
	
	// デストラクタ
	virtual ~MainLayer();
	
	// 初期化
	virtual bool init();


	// updateハンドラ
	virtual void update(float dt);
};
