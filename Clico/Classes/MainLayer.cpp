#include "SimpleAudioEngine.h"

#include "MainLayer.h"
#include "Clico.h"
#include "JellyFish.h"
#include "Util.h"

using namespace cocos2d;


// シーンを作成
Scene *MainLayer::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::createWithPhysics();

	// 'layer' is an autorelease object
	auto layer = MainLayer::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// PhysicsWorldを設定
	layer->initPhysics(scene->getPhysicsWorld());

	// return the scene
	return scene;
}


// デストラクタ
MainLayer::~MainLayer()
{
	// メンバを解放
	if (m_mouseStatus != nullptr) m_mouseStatus->release();
	if (m_keyStatus != nullptr) m_keyStatus->release();
	if (m_scrollStatus != nullptr) m_scrollStatus->release();
}


// 初期化
bool MainLayer::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// メンバを初期化
	m_mouseStatus = MouseStatus::create();
	m_mouseStatus->retain();
	m_keyStatus = KeyStatus::create();
	m_keyStatus->retain();
	m_scrollStatus = ScrollStatus::create();
	m_scrollStatus->retain();

	// updateをスケジュール
	this->scheduleUpdate();

	// マウスリスナーを登録
	auto mouseListener = EventListenerMouse::create();
	mouseListener->onMouseDown = CC_CALLBACK_1(MainLayer::onMouseDown, this);
	mouseListener->onMouseUp = CC_CALLBACK_1(MainLayer::onMouseUp, this);
	mouseListener->onMouseMove = CC_CALLBACK_1(MainLayer::onMouseMove, this);
	mouseListener->onMouseScroll = CC_CALLBACK_1(MainLayer::onMouseScroll, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mouseListener, this);

	// キーリスナーを登録
	auto keyListener = EventListenerKeyboard::create();
	keyListener->onKeyPressed = CC_CALLBACK_2(MainLayer::onKeyPressed, this);
	keyListener->onKeyReleased = CC_CALLBACK_2(MainLayer::onKeyReleased, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyListener, this);

	// ウィンドウサイズを取得
	auto origin = Director::getInstance()->getVisibleOrigin();
	auto size = Director::getInstance()->getVisibleSize();

	// テスト用
	auto clico = Clico::create();
	clico->setPosition(size.width / 2, size.height / 2);
	clico->setTag(1);
	this->addChild(clico);
	clico->addStatus((int)ClicoStatus::CONTROLLABLE);
	clico->setMouseStatus(m_mouseStatus);
	clico->setKeyStatus(m_keyStatus);
	clico->setScrollStatus(m_scrollStatus);

	auto jelly = JellyFish::create();
	jelly->setPosition(100, 100);
	this->addChild(jelly);
	jelly->addStatus((int)JellyFishStatus::NORMAL);

	//CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(0.2F);
	//CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("sound/Grand Blue.mp3");
	//CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("sound/Grand Blue.mp3", true);

	return true;
}


// 物理を初期化
void MainLayer::initPhysics(cocos2d::PhysicsWorld *physicsWorld)
{
	if (physicsWorld == nullptr)
	{
		return;
	}

	m_physicsWorld = physicsWorld;

	m_physicsWorld->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	m_physicsWorld->setGravity(Vec2(0.0F, 0.0F));
}


// updateハンドラ
void MainLayer::update(float dt)
{
	// 画面をスクロール
	auto clico = this->getChildByTag(1);
	Util::scrollLayer(this, clico->getPosition() - Director::getInstance()->getVisibleSize() / 2.0F);
	m_scrollStatus->setOrigin(-this->getPosition());
}


// MouseDownイベントハンドラ
void MainLayer::onMouseDown(Event *event)
{
	auto mouse = (EventMouse *)event;

	switch (mouse->getMouseButton())
	{
	case 0:  // LeftButton
		m_mouseStatus->setLeftButtonDown(true);
		break;

	case 1:  // RightButton
		m_mouseStatus->setRightButtonDown(true);
		break;

	case 2:  // MiddleButton
		break;
	}
}


// MouseUpイベントハンドラ
void MainLayer::onMouseUp(Event *event)
{
	auto mouse = (EventMouse *)event;

	switch (mouse->getMouseButton())
	{
	case 0:  // LeftButton
		m_mouseStatus->setLeftButtonDown(false);
		break;

	case 1:  // RightButton
		m_mouseStatus->setRightButtonDown(false);
		break;

	case 2:  // MiddleButton
		break;
	}
}


// MouseMoveイベントハンドラ
void MainLayer::onMouseMove(Event *event)
{
	auto mouse = (EventMouse *)event;
	
	m_mouseStatus->addPosition(Vec2(mouse->getCursorX(), mouse->getCursorY()));
}


// MouseScrollイベントハンドラ
void MainLayer::onMouseScroll(Event *event)
{
}


// KeyPressedイベントハンドラ
void MainLayer::onKeyPressed(EventKeyboard::KeyCode key, Event *event)
{
	m_keyStatus->setKeyDown(key, true);
}


// KeyReleasedイベントハンドラ
void MainLayer::onKeyReleased(EventKeyboard::KeyCode key, Event *event)
{
	m_keyStatus->setKeyDown(key, false);
}
