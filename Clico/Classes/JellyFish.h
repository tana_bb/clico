#pragma once

#include "cocos2d.h"
#include "Character.h"


enum class JellyFishSprite
{
	MAIN
};


enum class JellyFishAction
{
};


enum class JellyFishAnimation
{
	NORMAL = 1
};


enum class JellyFishStatus
{
	NORMAL = 0x1
};


// クラゲ
class JellyFish : public Character
{
private:
	// コンストラクタ
	JellyFish()
		: Character()
	{
	}


protected:
	// 通常状態のハンドラ
	virtual void func_normal(float dt);


public:
	// ファクトリメソッド
	CREATE_FUNC(JellyFish);

	// デストラクタ
	virtual ~JellyFish();


	// 初期化
	bool init();


	// 状態を追加
	virtual void addStatus(int types);
	
	// 状態を除去
	virtual void removeStatus(int types);
};
