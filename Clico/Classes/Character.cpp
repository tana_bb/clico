#include "Character.h"

using namespace cocos2d;


// デストラクタ
Character::~Character()
{
	// 全アクションを停止
	for (auto &sprite : m_sprites)
	{
		if (sprite.second != nullptr)
		{
			sprite.second->stopAllActions();
		}
	}

	// アニメーションを解放
	for (auto &m : m_animations)
	{
		for (auto &anime : m.second)
		{
			if (anime.second != nullptr)
			{
				anime.second->release();
			}
		}
	}

	// アクションを解放
	for (auto &action : m_actions)
	{
		if (action.second != nullptr)
		{
			action.second->release();
		}
	}

	// スプライトを解放
	for (auto &sprite : m_sprites)
	{
		if (sprite.second != nullptr)
		{
			sprite.second->release();
		}
	}
}


// 初期化
bool Character::init()
{
	if (!Sprite::init())
	{
		return false;
	}

	return true;
}


// アクションを追加
void Character::addAction(int types)
{
	for (auto i = 0x1; i < FLAG_MAX; i <<= 1)
	{
		if (types & i)
		{
			if (m_currentAction & i)
			{
				continue;
			}

			m_currentAction |= i;

			if (m_actions[i] != nullptr)
			{
				this->runAction(m_actions[i]);
			}
		}
	}
}


// アクションを除去
void Character::removeAction(int types)
{
	for (auto i = 0x1; i < FLAG_MAX; i <<= 1)
	{
		if (types & i)
		{
			if (!(m_currentAction & i))
			{
				continue;
			}

			m_currentAction &= ~i;

			if (m_actions[i] != nullptr)
			{
				this->stopAction(m_actions[i]);
			}
		}
	}
}


// アニメーションを設定
void Character::setAnimation(int type)
{
	if (m_currentAnimation == type)
	{
		return;
	}

	m_currentAnimation = type;

	for (auto &sprite : m_sprites)
	{
		if (sprite.second == nullptr)
		{
			continue;
		}

		// アニメーションを停止
		for (auto &a : m_animations[sprite.first])
		{
			if (a.second != nullptr)
			{
				sprite.second->stopAction(a.second);
			}
		}

		// アニメーションを設定
		auto anime = m_animations[sprite.first][type];
		if (anime != nullptr)
		{
			sprite.second->runAction(anime);
		}
	}
}


// 状態を追加
void Character::addStatus(int types)
{
}


// 状態を除去
void Character::removeStatus(int types)
{
}
