#pragma once

#include "cocos2d.h"
#include <vector>


#define POS_COUNT_MAX 10


// マウス状態
class MouseStatus : public cocos2d::Object
{
private:
	std::vector<cocos2d::Vec2> m_positions;  // 直近のマウス座標群を格納
	bool m_isLeftButtonDown;  // 左ボタンの押下状態
	bool m_isRightButtonDown;  // 右ボタンの押下状態

	// コンストラクタ
	MouseStatus()
		: cocos2d::Object()
		, m_positions()
		, m_isLeftButtonDown(false)
		, m_isRightButtonDown(false)
	{
	}

public:
	// ファクトリメソッド
	CREATE_FUNC(MouseStatus);

	// 初期化
	virtual bool init();


	// 全マウス座標を取得
	std::vector<cocos2d::Vec2> getPositions() const;
	
	// マウス座標を追加
	void addPosition(cocos2d::Vec2 position);


	// 左ボタンの押下状態を取得
	bool isLeftButtonDown() const;

	// 左ボタンの押下状態を設定
	void setLeftButtonDown(bool value);


	// 右ボタンの押下状態を取得
	bool isRightButtonDown() const;
	
	// 右ボタンの押下状態を設定
	void setRightButtonDown(bool value);


	// 最新のマウス座標を取得
	cocos2d::Vec2 getPosition() const;
	
	// 最新のマウス移動ベクトルを取得
	cocos2d::Vec2 getDelta() const;
};
