#include "JellyFish.h"

using namespace cocos2d;


JellyFish::~JellyFish()
{
}


bool JellyFish::init()
{
	if (!Character::init())
	{
		return false;
	}

	// スプライトを作成
	m_sprites[(int)JellyFishSprite::MAIN] = Sprite::create("enemy/jellyfish.png");
	m_sprites[(int)JellyFishSprite::MAIN]->retain();
	m_sprites[(int)JellyFishSprite::MAIN]->setTag((int)JellyFishSprite::MAIN);
	this->addChild(m_sprites[(int)JellyFishSprite::MAIN]);

	// アニメーションを作成
	auto anime_normal1 = ScaleTo::create(0.5F, 1.1F, 0.9F);
	auto anime_normal2 = ScaleTo::create(0.5F, 0.9F, 1.1F);
	auto anime_normal_seq = Sequence::create(anime_normal1, anime_normal2, nullptr);
	m_animations[(int)JellyFishSprite::MAIN][(int)JellyFishAnimation::NORMAL] = RepeatForever::create(anime_normal_seq);
	m_animations[(int)JellyFishSprite::MAIN][(int)JellyFishAnimation::NORMAL]->retain();

	// 剛体を設定
	auto physicsBody = PhysicsBody::createBox(m_sprites[(int)JellyFishSprite::MAIN]->getContentSize() * 0.6F);
	this->setPhysicsBody(physicsBody);

	// 攻撃力を設定
	m_attackValue = 1.0F;

	return true;
}


void JellyFish::addStatus(int types)
{
	for (auto i = 0x1; i < FLAG_MAX; i <<= 1)
	{
		switch (types & i)
		{
		case (int)JellyFishStatus::NORMAL:
			m_status |= (int)JellyFishStatus::NORMAL;
			this->schedule(schedule_selector(JellyFish::func_normal));
			break;
		}
	}
}


void JellyFish::removeStatus(int types)
{
	for (auto i = 0x1; i < FLAG_MAX; i <<= 1)
	{
		switch (types & i)
		{
		case (int)JellyFishStatus::NORMAL:
			m_status &= ~(int)JellyFishStatus::NORMAL;
			this->unschedule(schedule_selector(JellyFish::func_normal));
			break;
		}
	}
}


void JellyFish::func_normal(float dt)
{
	this->setAnimation((int)JellyFishAnimation::NORMAL);

	auto physics = this->getPhysicsBody();
	physics->setVelocity(Vec2(0.0F, 0.0F));
	physics->setAngularVelocity(0.0F);
}
