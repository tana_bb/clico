#include "Clico.h"
#include "Util.h"

using namespace cocos2d;


// デストラクタ
Clico::~Clico()
{
	// メンバの解放
	if (m_mouseStatus != nullptr) m_mouseStatus->release();
	if (m_keyStatus != nullptr) m_keyStatus->release();
	if (m_scrollStatus != nullptr) m_scrollStatus->release();
	if (m_tentacle != nullptr) m_tentacle->release();
}


// 初期化
bool Clico::init()
{
	if (!Sprite::init())
	{
		return false;
	}

	// ClicoTentacleを作成
	m_tentacle = ClicoTentacle::create();
	m_tentacle->retain();
	this->addChild(m_tentacle);

	// スプライトを作成
	m_sprites[(int)ClicoSprite::FIN] = Sprite::create("clione/clione_fin_01.png");
	m_sprites[(int)ClicoSprite::FIN]->retain();
	m_sprites[(int)ClicoSprite::FIN]->setTag((int)ClicoSprite::FIN);
	this->addChild(m_sprites[(int)ClicoSprite::FIN], 0);

	m_sprites[(int)ClicoSprite::HEAD] = Sprite::create("clione/clione_head_01.png");
	m_sprites[(int)ClicoSprite::HEAD]->retain();
	m_sprites[(int)ClicoSprite::HEAD]->setTag((int)ClicoSprite::HEAD);
	this->addChild(m_sprites[(int)ClicoSprite::HEAD], 0);

	m_sprites[(int)ClicoSprite::BODY] = Sprite::create("clione/clione_body_01.png");
	m_sprites[(int)ClicoSprite::BODY]->retain();
	m_sprites[(int)ClicoSprite::BODY]->setTag((int)ClicoSprite::BODY);
	this->addChild(m_sprites[(int)ClicoSprite::BODY], 0);

	m_sprites[(int)ClicoSprite::HEART] = Sprite::create("clione/clione_heart_01.png");
	m_sprites[(int)ClicoSprite::HEART]->retain();
	m_sprites[(int)ClicoSprite::HEART]->setTag((int)ClicoSprite::HEART);
	this->addChild(m_sprites[(int)ClicoSprite::HEART], 0);

	m_sprites[(int)ClicoSprite::CHEEK] = Sprite::create("clione/clione_cheek_01.png");
	m_sprites[(int)ClicoSprite::CHEEK]->retain();
	m_sprites[(int)ClicoSprite::CHEEK]->setTag((int)ClicoSprite::CHEEK);
	this->addChild(m_sprites[(int)ClicoSprite::CHEEK], 0);

	m_sprites[(int)ClicoSprite::EYE] = Sprite::create("clione/clione_eye_01.png");
	m_sprites[(int)ClicoSprite::EYE]->retain();
	m_sprites[(int)ClicoSprite::EYE]->setTag((int)ClicoSprite::EYE);
	this->addChild(m_sprites[(int)ClicoSprite::EYE], 0);

	// finのアニメーションを作成
	auto finAnime_normal = Animation::create();
	for (auto i = 1; i <= 12; ++i)
	{
		char name[100] = { 0 };
		sprintf(name, "clione/clione_fin_%02d.png", i);
		finAnime_normal->addSpriteFrameWithFileName(name);
	}
	finAnime_normal->setDelayPerUnit(4.0F / 60.0F);
	m_animations[(int)ClicoSprite::FIN][(int)ClicoAnimation::NORMAL] = RepeatForever::create(Animate::create(finAnime_normal));
	m_animations[(int)ClicoSprite::FIN][(int)ClicoAnimation::NORMAL]->retain();

	auto finAnime_move = finAnime_normal->clone();
	finAnime_move->setDelayPerUnit(1.0F / 60.0F);
	m_animations[(int)ClicoSprite::FIN][(int)ClicoAnimation::MOVE] = RepeatForever::create(Animate::create(finAnime_move));
	m_animations[(int)ClicoSprite::FIN][(int)ClicoAnimation::MOVE]->retain();

	auto finAnime_eat = finAnime_normal->clone();
	finAnime_eat->setDelayPerUnit(1.0F / 60.0F);
	m_animations[(int)ClicoSprite::FIN][(int)ClicoAnimation::EAT] = RepeatForever::create(Animate::create(finAnime_eat));
	m_animations[(int)ClicoSprite::FIN][(int)ClicoAnimation::EAT]->retain();

	// bodyのアニメーションを作成
	auto bodyAnime_normal = Animation::create();
	for (auto i = 1; i <= 20; ++i)
	{
		char name[100] = { 0 };
		sprintf(name, "clione/clione_body_%02d.png", i);
		bodyAnime_normal->addSpriteFrameWithFileName(name);
	}
	bodyAnime_normal->setDelayPerUnit(8.0F / 60.0F);
	m_animations[(int)ClicoSprite::BODY][(int)ClicoAnimation::NORMAL] = RepeatForever::create(Animate::create(bodyAnime_normal));
	m_animations[(int)ClicoSprite::BODY][(int)ClicoAnimation::NORMAL]->retain();

	auto bodyAnime_move = bodyAnime_normal->clone();
	bodyAnime_move->setDelayPerUnit(2.0F / 60.0F);
	m_animations[(int)ClicoSprite::BODY][(int)ClicoAnimation::MOVE] = RepeatForever::create(Animate::create(bodyAnime_move));
	m_animations[(int)ClicoSprite::BODY][(int)ClicoAnimation::MOVE]->retain();

	auto bodyAnime_eat = bodyAnime_normal->clone();
	bodyAnime_eat->setDelayPerUnit(2.0F / 60.0F);
	m_animations[(int)ClicoSprite::BODY][(int)ClicoAnimation::EAT] = RepeatForever::create(Animate::create(bodyAnime_eat));
	m_animations[(int)ClicoSprite::BODY][(int)ClicoAnimation::EAT]->retain();

	// headのアニメーションを作成
	auto headAnime_normal = Animation::create();
	headAnime_normal->addSpriteFrameWithFileName("clione/clione_head_01.png");
	headAnime_normal->setDelayPerUnit(60.0F / 60.0F);
	m_animations[(int)ClicoSprite::HEAD][(int)ClicoAnimation::NORMAL] = RepeatForever::create(Animate::create(headAnime_normal));
	m_animations[(int)ClicoSprite::HEAD][(int)ClicoAnimation::NORMAL]->retain();

	auto headAnime_move = headAnime_normal->clone();
	m_animations[(int)ClicoSprite::HEAD][(int)ClicoAnimation::MOVE] = RepeatForever::create(Animate::create(headAnime_move));
	m_animations[(int)ClicoSprite::HEAD][(int)ClicoAnimation::MOVE]->retain();

	auto headAnime_eat1 = Animation::create();
	for (auto i = 1; i <= 3; ++i)
	{
		char name[100] = { 0 };
		sprintf(name, "clione/clione_head_eat_%02d.png", i);
		headAnime_eat1->addSpriteFrameWithFileName(name);
	}
	headAnime_eat1->setDelayPerUnit(8.0F / 60.0F);

	auto headAnime_eat2 = Animation::create();
	headAnime_eat2->addSpriteFrameWithFileName("clione/clione_head_eat_03.png");
	headAnime_eat2->setDelayPerUnit(42.0F / 60.0F);
	
	auto headAnime_eat3 = Animation::create();
	for (auto i = 3; i >= 1; --i)
	{
		char name[100] = { 0 };
		sprintf(name, "clione/clione_head_eat_%02d.png", i);
		headAnime_eat3->addSpriteFrameWithFileName(name);
	}
	headAnime_eat3->setDelayPerUnit(8.0F / 60.0F);
	
	m_animations[(int)ClicoSprite::HEAD][(int)ClicoAnimation::EAT] =
		Sequence::create(Animate::create(headAnime_eat1), Animate::create(headAnime_eat2), Animate::create(headAnime_eat3), nullptr);
	m_animations[(int)ClicoSprite::HEAD][(int)ClicoAnimation::EAT]->retain();

	// cheekのアニメーションを作成
	m_animations[(int)ClicoSprite::CHEEK][(int)ClicoAnimation::EAT] =
		Sequence::create(FadeOut::create(24.0F / 60.0F), DelayTime::create(42.0F / 60.0F), FadeIn::create(24.0F / 60.0F), nullptr);
	m_animations[(int)ClicoSprite::CHEEK][(int)ClicoAnimation::EAT]->retain();

	// eyeのアニメーションを作成
	m_animations[(int)ClicoSprite::EYE][(int)ClicoAnimation::EAT] =
		Sequence::create(FadeOut::create(24.0F / 60.0F), DelayTime::create(42.0F / 60.0F), FadeIn::create(24.0F / 60.0F), nullptr);
	m_animations[(int)ClicoSprite::EYE][(int)ClicoAnimation::EAT]->retain();

	// 剛体を設定
	auto physicsBody = PhysicsBody::createBox(m_sprites[(int)ClicoSprite::HEAD]->getContentSize() * 0.6F);
	this->setPhysicsBody(physicsBody);
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(Clico::onPhysicsContact, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	return true;
}


// 状態を追加
void Clico::addStatus(int types)
{
	for (auto i = 0x1; i < FLAG_MAX; i <<= 1)
	{
		switch (types & i)
		{
		case (int)ClicoStatus::CONTROLLABLE:
			m_status |= (int)ClicoStatus::CONTROLLABLE;
			removeStatus((int)ClicoStatus::DAMAGE);
			this->schedule(schedule_selector(Clico::func_controllable));
			break;

		case (int)ClicoStatus::DAMAGE:
			m_status |= (int)ClicoStatus::DAMAGE;
			removeStatus((int)ClicoStatus::CONTROLLABLE);
			break;

		case (int)ClicoStatus::EAT:
			m_status |= (int)ClicoStatus::EAT;
			this->schedule(schedule_selector(Clico::func_eat));
			break;
		}
	}
}


// 状態を除去
void Clico::removeStatus(int types)
{
	for (auto i = 0x1; i < FLAG_MAX; i <<= 1)
	{
		switch (types & i)
		{
		case (int)ClicoStatus::CONTROLLABLE:
			m_status &= ~(int)ClicoStatus::CONTROLLABLE;
			this->unschedule(schedule_selector(Clico::func_controllable));
			break;

		case (int)ClicoStatus::DAMAGE:
			m_status &= ~(int)ClicoStatus::DAMAGE;
			break;

		case (int)ClicoStatus::EAT:
			m_status &= ~(int)ClicoStatus::EAT;
			this->unschedule(schedule_selector(Clico::func_eat));
			break;
		}
	}
}


// PhysicsContactのイベントハンドラ
bool Clico::onPhysicsContact(cocos2d::PhysicsContact &contact)
{
	CCLOG("physics contact");

	return true;
}


// 操作可能状態のハンドラ
void Clico::func_controllable(float dt)
{
	auto physicsBody = this->getPhysicsBody();
	physicsBody->setVelocityLimit(m_moveVelLimit);
	physicsBody->setAngularVelocityLimit(m_moveRotLimit);

	if (m_keyStatus->isKeyDown(EventKeyboard::KeyCode::KEY_SPACE))  // 移動する場合
	{
		if (!(m_status & (int)ClicoStatus::EAT))
		{
			this->setAnimation((int)ClicoAnimation::MOVE);
		}
		
		Util::moveNode(this, m_mouseStatus->getPosition() + m_scrollStatus->getOrigin(), m_moveAccel, m_moveRotSpeed, dt);
	}
	else  // 移動しない場合
	{
		if (!(m_status & (int)ClicoStatus::EAT))
		{
			this->setAnimation((int)ClicoAnimation::NORMAL);
		}
		
		Util::stopNode(this, m_stopAccel, dt);
	}

	// 捕食
	if (m_mouseStatus->isLeftButtonDown() && !(m_status & (int)ClicoStatus::EAT))
	{
		this->addStatus((int)ClicoStatus::EAT);
		this->setAnimation((int)ClicoAnimation::EAT);

		m_tentacle->addStatus((int)ClicoTentacleStatus::EAT);
		m_tentacle->setAnimation((int)ClicoTentacleAnimation::EAT);
	}
}


// 捕食状態のハンドラ
void Clico::func_eat(float dt)
{
	if (m_animations[(int)ClicoSprite::HEAD][(int)ClicoAnimation::EAT] == nullptr
		|| m_animations[(int)ClicoSprite::HEAD][(int)ClicoAnimation::EAT]->isDone())
	{
		this->removeStatus((int)ClicoStatus::EAT);
		this->setAnimation((int)ClicoAnimation::NORMAL);
	}
}


// マウス状態を設定
void Clico::setMouseStatus(MouseStatus *status)
{
	if (m_mouseStatus != nullptr)
	{
		m_mouseStatus->release();
	}

	m_mouseStatus = status;

	if (m_mouseStatus != nullptr)
	{
		m_mouseStatus->retain();
	}
}


// キー状態を設定
void Clico::setKeyStatus(KeyStatus *status)
{
	if (m_keyStatus != nullptr)
	{
		m_keyStatus->release();
	}

	m_keyStatus = status;

	if (m_keyStatus != nullptr)
	{
		m_keyStatus->retain();
	}
}


// スクロール状態を設定
void Clico::setScrollStatus(ScrollStatus *status)
{
	if (m_scrollStatus != nullptr)
	{
		m_scrollStatus->release();
	}

	m_scrollStatus = status;

	if (m_scrollStatus != nullptr)
	{
		m_scrollStatus->retain();
	}
}
