#include "AppLayer.h"
#include "MainLayer.h"

using namespace cocos2d;


// シーンを作成
Scene *AppLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = AppLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool AppLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

	// 次のシーンに移る
	this->scheduleOnce(schedule_selector(AppLayer::nextScene), 0.0F);
    
    return true;
}


// 次のシーンに移る
void AppLayer::nextScene(float dt)
{
	auto scene = TransitionFade::create(1.0F, MainLayer::createScene());
	Director::getInstance()->replaceScene(scene);
}
