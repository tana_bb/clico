#pragma once

#include "cocos2d.h"


// 各種機能
class Util
{
public:
	// ノードを移動
	static void moveNode(
		cocos2d::Node *node,
		cocos2d::Vec2 position,
		float accel,
		float rotSpeed,
		float dt);

	// ノードを停止
	static void stopNode(
		cocos2d::Node *node,
		float accel,
		float dt);


	// レイヤーをスクロール
	static void scrollLayer(
		cocos2d::Layer *layer,
		cocos2d::Vec2 origin);
};
