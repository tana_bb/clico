#include "Util.h"

using namespace cocos2d;


// ノードを移動
void Util::moveNode(
	Node *node,
	Vec2 position,
	float accel,
	float rotSpeed,
	float dt)
{
	// 剛体を取得
	auto physicsBody = node->getPhysicsBody();

	// マウス座標とスプライト座標の差を計算
	auto dx = position.x - node->getPositionX();
	auto dy = position.y - node->getPositionY();
	auto dv = Vec2(dx, dy);

	// 移動処理
	if (!isnan(dv.x) && !isnan(dv.y) && !dv.isZero())
	{
		dv.normalize();
		physicsBody->applyImpulse(dv * accel);

		// 回転
		float currentRot = fmod(node->getRotation() + 360.0F, 360.0F);
		float targetRot = fmod(atan2f(dy, dx) / M_PI * -180.0F + 90.0F + 360.0F, 360.0F);

		if (fabsf(targetRot - currentRot) <= rotSpeed * dt)  // 回転角が小さいとき
		{
			node->setRotation(targetRot);
		}
		else  // 回転角が大きいとき
		{
			float speed;
			if (targetRot - currentRot > 180.0F)
			{
				speed = -rotSpeed;
			}
			else if (targetRot - currentRot < -180.0F)
			{
				speed = rotSpeed;
			}
			else
			{
				speed = targetRot - currentRot >= 0.0F ? rotSpeed : -rotSpeed;
			}

			node->setRotation(currentRot + speed * dt);
		}
	}
}


// ノードを停止
void Util::stopNode(
	Node *node,
	float accel,
	float dt)
{
	auto physicsBody = node->getPhysicsBody();

	auto vel = -physicsBody->getVelocity();
	vel.normalize();
	
	physicsBody->applyImpulse(vel * accel);
	physicsBody->setAngularVelocity(0.0F);
}


// レイヤーをスクロール
void Util::scrollLayer(
	Layer *layer,
	Vec2 origin)
{
	layer->setPosition(-origin);
}
