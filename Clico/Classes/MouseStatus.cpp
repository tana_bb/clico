#include "MouseStatus.h"

using namespace cocos2d;


// 初期化
bool MouseStatus::init()
{
	return true;
}


// 全マウス座標を取得
std::vector<cocos2d::Vec2> MouseStatus::getPositions() const
{
	return m_positions;
}


// マウス座標を追加
void MouseStatus::addPosition(cocos2d::Vec2 position)
{
	if (m_positions.size() > POS_COUNT_MAX)
	{
		m_positions.pop_back();
	}

	m_positions.insert(m_positions.begin(), position);
}


// 左ボタンの押下状態を取得
bool MouseStatus::isLeftButtonDown() const
{
	return m_isLeftButtonDown;
}


// 左ボタンの押下状態を設定
void MouseStatus::setLeftButtonDown(bool value)
{
	m_isLeftButtonDown = value;
}


// 右ボタンの押下状態を取得
bool MouseStatus::isRightButtonDown() const
{
	return m_isRightButtonDown;
}


// 右ボタンの押下状態を設定
void MouseStatus::setRightButtonDown(bool value)
{
	m_isRightButtonDown = value;
}


// 最新のマウス座標を取得
Vec2 MouseStatus::getPosition() const
{
	return m_positions.size() > 0 ? m_positions[0] : Vec2(NAN, NAN);
}


// 最新のマウス移動ベクトルを取得
Vec2 MouseStatus::getDelta() const
{
	return m_positions.size() >= 2 ? m_positions[0] - m_positions[1] : Vec2(NAN, NAN);
}
